"use strict"

// All form fields
const forms = document.getElementsByClassName("form__content");

// All "Next" buttons
// Pretty hardcoded, can be done better without swtch case statements
const nextButtons = document.querySelectorAll(".btn-next");
nextButtons.forEach(btn => btn.addEventListener("click", function(e) {
	switch (e.target.id[e.target.id.length - 1]) {
		case "1":
			forms[0].classList.add("done");
			forms[1].classList.remove("next");
			break;
		case "2":
			forms[1].classList.add("done");
			forms[2].classList.remove("next");
			break;
		case "3":
			forms[2].classList.add("done");
			forms[3].classList.remove("next");
			break;
		case "4":
			forms[3].classList.add("done");
			forms[4].classList.remove("next");
			break;
		case "5":
			forms[4].classList.add("done");
			forms[5].classList.remove("next");
			break;
		case "6":
			forms[5].classList.add("done");
			forms[6].classList.remove("next");
			break;
		case "7":
			forms[6].classList.add("done");
			forms[7].classList.remove("next");
			break;
	}
}));

// All "Back" buttons
// Pretty hardcoded, can be done better without swtch case statements
const backButtons = document.querySelectorAll(".btn-back");
backButtons.forEach(btn => btn.addEventListener("click", function(e) {
	switch (e.target.id[e.target.id.length - 1]) {
		case "2":
			forms[1].classList.add("next");
			forms[0].classList.remove("done");
			break;
		case "3":
			forms[2].classList.add("next");
			forms[1].classList.remove("done");
			break;
		case "4":
			forms[3].classList.add("next");
			forms[2].classList.remove("done");
			break;
		case "5":
			forms[4].classList.add("next");
			forms[3].classList.remove("done");
			break;
		case "6":
			forms[5].classList.add("next");
			forms[4].classList.remove("done");
			break;
		case "7":
			forms[6].classList.add("next");
			forms[5].classList.remove("done");
			break;
	}
}));
